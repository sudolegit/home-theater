#!/bin/bash
####################################################################################################
# @file
# 
# @brief			Experimental file used to explore bit banging SPI communication to the external 
#					ADC chip.
####################################################################################################


#===================================================================================================
# PROCESS USER INPUT TO SCRIPT
#---------------------------------------------------------------------------------------------------
if [ $# -ge 1 ]; then
	min=${1}
else
	min=1
fi

if [ $# -ge 2 ]; then
	max=${2}
else
	max=41
fi

if [ $# -ge 3 ]; then
	count=${3}
else
	count=6
fi


#===================================================================================================
# TEST ALL POSSIBLE GPIO LINES
#---------------------------------------------------------------------------------------------------
echo "Will Loop through all GPIO options from {${min}, ${max}}. Each loop will set the GPIO as an output and toggle it '${count}' times."
for g in $(seq ${min} ${max}); do
	
	# Ensure GPIO entry exists.
	if ! [ -d /sys/class/gpio/gpio${g} ]; then
		echo "Creating GPIO ${g}."
		echo ${g} > /sys/class/gpio/export
		flag_manually_created=1
	else
		echo "GPIO ${g} exists. Proceeding."
		flag_manually_created=0
	fi
	
	echo "out" > /sys/class/gpio/gpio${g}/direction
	

	for c in $(seq 1 ${count}); do
		echo $((1 - $(cat /sys/class/gpio/gpio${g}/value) )) > /sys/class/gpio/gpio${g}/value 
		echo "GPIO-${g}:  $(cat /sys/class/gpio/gpio${g}/value)"
		sleep 1
	done

	# Remove current GPIO entry if we created it.
	if [ ${flag_manually_created} -eq 1 ]; then 
		echo "Removing manually created GPIO '${g}'."
		echo ${g} > /sys/class/gpio/unexport
	fi
	
	echo "Moving onto next GPIO"
	
done


