#!/bin/bash
####################################################################################################
# @file
# 
# @brief			Simple script to toggle GPIO pins to determine how to potentially hard-code 
#					[bit-bang] external I/O.
####################################################################################################


# Ensure one GPIO value was requested. Create the GPIO if it does not exist.
if ! [ $# -eq 1 ]; then
	echo "Please provide one GPIO value."
	exit 1
elif ! [ -d /sys/class/gpio/gpio${1} ]; then
	echo "Creating GPIO ${1}."
	echo ${1} > /sys/class/gpio/export
else
	echo "GPIO ${1} exists. Proceeding."
fi

# Ensure GPIO is an output.
echo "out" > /sys/class/gpio/gpio${1}/direction

# Loop through and toggle GPIO forever [allows probing externally on scope or DMM].
while true; do
	echo $((1 - $(cat /sys/class/gpio/gpio${1}/value) )) > /sys/class/gpio/gpio${1}/value 
	cat /sys/class/gpio/gpio${1}/value
	sleep 1
done


