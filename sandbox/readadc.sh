#!/bin/bash
####################################################################################################
# @file
# 
# @brief			Experimental file used to explore bit banging SPI communication to the external 
#					ADC chip.
# 
# @param[in]		ADC-Channel			Channel on external ADC that should be read in (should be 
#										{1,8}).
####################################################################################################


#===================================================================================================
# DEFINE CONSTANTS USED IN SCRIPT
#---------------------------------------------------------------------------------------------------
MOSI=10
MISO=9
CLK=11
CS=25

HIGH=1
LOW=0

PATH_GPIO_DIR=/sys/class/gpio

MAX_ADC_CHAN=7




#===================================================================================================
# INITIALIZE GPIO CONNECTED TO EXTERNAL ADC
#---------------------------------------------------------------------------------------------------
# Configure MOSI:
echo -n "Configuring MOSI for GPIO ${MOSI} ... "
if ! [ -d ${PATH_GPIO_DIR}/gpio${MOSI} ]; then 
	echo -n "creating entry ... "
	echo ${MOSI} > ${PATH_GPIO_DIR}/export
fi
echo -n "forcing output direction ... "
echo "out" > ${PATH_GPIO_DIR}/gpio${MOSI}/direction
echo "DONE."

# Configure MISO:
echo -n "Configuring MISO for GPIO ${MISO} ... "
if ! [ -d ${PATH_GPIO_DIR}/gpio${MISO} ]; then 
	echo -n "creating entry ... "
	echo ${MISO} > ${PATH_GPIO_DIR}/export
fi
echo -n "forcing input direction ... "
echo "in" > ${PATH_GPIO_DIR}/gpio${MISO}/direction
echo "DONE."

# Configure CLK:
echo -n "Configuring CLK for GPIO ${CLK} ... "
if ! [ -d ${PATH_GPIO_DIR}/gpio${CLK} ]; then 
	echo -n "creating entry ... "
	echo ${CLK} > ${PATH_GPIO_DIR}/export
fi
echo -n "forcing output direction ... "
echo "out" > ${PATH_GPIO_DIR}/gpio${CLK}/direction
echo "DONE."

# Configure CS:
echo -n "Configuring CS for GPIO ${CS} ... "
if ! [ -d ${PATH_GPIO_DIR}/gpio${CS} ]; then 
	echo -n "creating entry ... "
	echo ${CS} > ${PATH_GPIO_DIR}/export
fi
echo -n "forcing output direction ... "
echo "out" > ${PATH_GPIO_DIR}/gpio${CS}/direction
echo "DONE."




####################################################################################################
# @brief			Push the provided value to the given GPIO.
# 
# @note				Presumes GPIO exists and is configured as an output.
# 
# @param[in]		GPIO-##				GPIO number [e.g. '1'] that should be modified.
# @param[in]		Value				Value to push to the GPIO pin.
####################################################################################################
function set_gpio_val()
{
	if [ $# -eq 2 ]; then
		echo "${2}" > ${PATH_GPIO_DIR}/gpio${1}/value
	fi
}




####################################################################################################
# @brief			Read the provided value of the requested GPIO.
# 
# @note				Presumes GPIO exists and is configured as an input.
# 
# @param[in]		GPIO-##				GPIO number [e.g. '1'] that should be read.
# 
# @retval			{0,1}				Value presently available on digital GPIO pin after reading.
####################################################################################################
function read_gpio_val()
{
	if [ $# -eq 1 ]; then
		cat ${PATH_GPIO_DIR}/gpio${1}/value
	fi
}




####################################################################################################
# @brief			Read the provided value of the requested ADC channel on the external ADC via 
#					SPI.
# 
# @details			Bit bang SPI communication with the following [rough] flow:
#						- Ensure CS is high.
#						- Bring clock low.
#						- Bring cs low.
#						- Push high 5-bits of (0x18 | Channel-##) out to the ADC by:
#							- Setting MOSI pin to match bit-field value.
#							- Toggling clock high and then low.
#						- Read in 12-Bit value from external 12-Bit ADC by:
#							- Toggling clock high and then low.
#							- Read value (0 or 1) on MISO line.
#							- Adding read in value to [12] bit-field representing value of ADC.
#						- Shift 12-bit value right by one to account for null read on first bit.
#						- Echo bit-field value to terminal.
#						- Set CS high.
# 
# @note				Presumes GPIO already initialized.
# 
# @param[in]		Channel-##			Channel number [e.g. '1'] that should be read.
# 
# @returns			Echos the current value of the ADC as an integer to the terminal.
####################################################################################################
function readadc()
{
	if [ $# -eq 1 ]; then
		chan="${1}"
	else
		chan=0
	fi
	
	if [ ${chan} -gt ${MAX_ADC_CHAN} ]; then 
		chan=0
	fi
	
	echo "Will attempt to read ADC channel '${chan}'."
	
	set_gpio_val ${CS} ${HIGH}
	set_gpio_val ${CLK} ${LOW}
	set_gpio_val ${CS} ${LOW}
	
	cmd=${chan}
	cmd=$(( ${cmd} | 0x18 ))
	cmd=$(( ${cmd} << 3 ))
	for i in $(seq 1 5); do
		
		# Prep MOSI bit.
		if [ $(( cmd & 0x80)) -gt 0 ]; then
			set_gpio_val ${MOSI} ${HIGH}
		else
			set_gpio_val ${MOSI} ${LOW}
		fi
		
		# Shift command string one bit left to prepare for next loop iteration.
		cmd=$(( ${cmd} << 1 ))
		
		# Toggle clock for one pulse.
		set_gpio_val ${CLK} ${HIGH}
		set_gpio_val ${CLK} ${LOW}
		
	done
	
	val=0
	for i in $(seq 1 12); do
		set_gpio_val ${CLK} ${HIGH}
		set_gpio_val ${CLK} ${LOW}
		val=$(( ${val} << 1 ))
		tmp=$( read_gpio_val ${MISO} )
		val=$(( ${val} | ${tmp} ))
	done
	
	val=$(( ${val} >> 1 ))
	
	echo "ADC Value == '${val}'."
	
	set_gpio_val ${CS} ${HIGH}
	
}




#===================================================================================================
# INVOKE HELPER FUNCTION TO READ THE ADC CHANNEL WITH THE FIRST PARAMETER PASSED TO THE SCRIPT
#---------------------------------------------------------------------------------------------------
readadc ${1}




