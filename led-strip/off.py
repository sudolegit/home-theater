####################################################################################################
# @file
# 
# @brief            Disable the LED strip by modifying the state of the GPIO pin used to control 
#                   the power to the LED strip.
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import subprocess                                                               # Used when invoking Linux commands from a shell session.
import config                                                                   # Imports all user configuration settings.


#===================================================================================================
# DISABLE LED STRIP BY SENDING SYSTEM COMMAND TO UPDATE EXTERNAL VALUE
#---------------------------------------------------------------------------------------------------
cmd     = "echo %s > %s/gpio%s/value" % (config.led_strip_disable, config.gpio_root_dir, config.led_strip_control_pin)
proc    = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
proc.communicate()


