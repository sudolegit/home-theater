####################################################################################################
# @file
# 
# @brief            Defines constants related to interacting with the external LED strip.
####################################################################################################


#===================================================================================================
#   DEFINE LED STRIP GPIO DETAILS
#---------------------------------------------------------------------------------------------------
led_strip_control_pin   = "23"                  # GPIO pin associated with transmitting data to LED strip.
led_strip_enable        = "0"                   # By convention, set enable line low to enable P-Ch MOSFET and the LED strip.
led_strip_disable       = "1"                   # By convention, set enable line high to disable P-Ch MOSFET and the LED strip.
led_strip_default_on    = True                  # Boolean flag indicating if the LED strip should start on or off.




#===================================================================================================
#   DEFINE GENERAL GPIO DETAILS
#---------------------------------------------------------------------------------------------------
gpio_root_dir           = "/sys/class/gpio"     # Root directory containing GPIO definitions on system.




