####################################################################################################
# @file
# 
# @brief            Initialize support for the external LED strip.
# 
# @details          Configure system GPIO and enable or disable the LED strip based on the current 
#                   settings in the configuration file.
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import subprocess                                                               # Used when invoking Linux commands from a shell session.
import config                                                                   # Imports all user configuration settings.


#===================================================================================================
# ENABLE THE LED STRIP GPIO
#---------------------------------------------------------------------------------------------------
cmd     = "echo %s > %s/export" % (config.led_strip_control_pin, config.gpio_root_dir)
proc    = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
proc.communicate()


#===================================================================================================
# CONFIGURE THE LED STRIP GPIO AS AN OUTPUT
#---------------------------------------------------------------------------------------------------
cmd     = "echo out > %s/gpio%s/direction" % (config.gpio_root_dir, config.led_strip_control_pin)
proc    = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
proc.communicate()


#===================================================================================================
# INVOKE THE APPROPRIATE EXTERNAL SCRIPT TO HANDLE ENABLING OR DISABLING THE LED STRIP BY DEFAULT
#---------------------------------------------------------------------------------------------------
if config.led_strip_default_on == True:
    import on
else:
    import off


