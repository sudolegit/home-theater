####################################################################################################
# @file
# 
# @brief            Reads in the current luminance gain level and decrements it by the adjustment 
#                   level defined in the configuration file until the required minimum (also in 
#                   configuration file) has been reached.
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import os, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))

import config                                                                   # Imports all user configuration settings.
import lib                                                                      # Load in Hyperion related library helper functions.


#===================================================================================================
# DEFINE SCRIPT CONSTANT(S)
#---------------------------------------------------------------------------------------------------
param_get   = "luminanceGain"                                                   # Getter string to use when querying value.
param_set   = "luminance"                                                       # Setter string to use when updating value.


#===================================================================================================
# DETERMINE NEW VALUE TO APPLY
#---------------------------------------------------------------------------------------------------
crnt_val    = lib.query_param(param_get)
new_val     = max( (float(crnt_val) - float(config.LUMINANCE_ADJ)), float(config.LUMINANCE_MIN) )


#===================================================================================================
# APPLY VALUE IF CHANGE DETECTED
#---------------------------------------------------------------------------------------------------
if crnt_val != new_val:
    lib.send_command( "%s --%s %s" % (config.HYPERION_REMOTE, param_set, new_val) )


