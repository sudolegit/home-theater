####################################################################################################
# @file
# 
# @brief            List the current and default value of all parameters that can be modified 
#                   (increased or decreased) via user input.
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import os, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))

import config                                                                   # Imports all user configuration settings.
import lib                                                                      # Load in Hyperion related library helper functions.


#===================================================================================================
# QUERY CURRENT AND DEFAULT VALUES TO PRESENT A COMPREHENSIVE LIST OF CUSTOMIZATIONS TO THE USER
#---------------------------------------------------------------------------------------------------
print "> Saturation Gain [Color Depth]:     %.4f  [default:  %.4f]" % (float(lib.query_param("saturationL")),   float(lib.query_param_default("saturationGain"))    )

print "> Luminance Gain  [Brightness]:      %.4f  [default:  %.4f]" % (float(lib.query_param("luminanceGain")), float(lib.query_param_default("luminanceGain"))     )

print "> Luminance Min   [Backlight]:       %.4f  [default:  %.4f]" % (float(lib.query_param("luminanceMin")),  float(lib.query_param_default("luminanceMinimum"))  )

gamma_crnt  = str(lib.query_param("gamma")).replace(",", "").split(" ")
gamma_def   = str(lib.query_param_default("gamma")).replace("\n", " ").split(" ")
print "> Gamma Red:                         %.4f  [default:  %.4f]" % (float(gamma_crnt[0]),  float(gamma_def[0])  )
print "> Gamma Green:                       %.4f  [default:  %.4f]" % (float(gamma_crnt[1]),  float(gamma_def[1])  )
print "> Gamma Blue:                        %.4f  [default:  %.4f]" % (float(gamma_crnt[2]),  float(gamma_def[2])  )


