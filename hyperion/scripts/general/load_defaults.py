####################################################################################################
# @file
# 
# @brief            Apply all default values that can be modifying (increased or decreased) via user 
#                   input.
# 
# @warning          Does not modify the current enable/disable state of the LED strip.
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import os, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))

import config                                                                   # Imports all user configuration settings.
import lib                                                                      # Load in Hyperion related library helper functions.


#===================================================================================================
# SET DEFAULT VALUE OF SATURATION GAIN [COLOR DEPTH]
#---------------------------------------------------------------------------------------------------
default = lib.query_param_default("saturationGain")
lib.send_command( "%s --saturationL %s" % (config.HYPERION_REMOTE, default) )


#===================================================================================================
# SET DEFAULT VALUE OF LUMINANCE LEVEL [BRIGHTNESS]
#---------------------------------------------------------------------------------------------------
default = lib.query_param_default("luminanceGain")
lib.send_command( "%s --luminance %s" % (config.HYPERION_REMOTE, default) )


#===================================================================================================
# SET DEFAULT VALUE OF LUMINANCE MINIMUM [BACKLIGHT]
#---------------------------------------------------------------------------------------------------
default = lib.query_param_default("luminanceMinimum")
lib.send_command( "%s --luminanceMin %s" % (config.HYPERION_REMOTE, default) )


#===================================================================================================
# SET DEFAULT VALUE OF [ALL] GAMMA VALUES
#---------------------------------------------------------------------------------------------------
default = str(lib.query_param_default("gamma")).replace("\n", " ")
lib.send_command( "%s --gamma '%s'" % (config.HYPERION_REMOTE, default) )


