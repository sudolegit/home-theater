####################################################################################################
# @file
# 
# @brief            Back's up and writes the value of all tunable settings to the Hyperion 
#                   configuration file.
# 
# @warning          Presumes file saves are not more frequent than once a second with unique file 
#                   changes. If somehow this is not true, the backup may be overridden.
# 
# @note             No checks are made to see if the current settings match the values of the 
#                   existing settings file (blind write to file to force saving).
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import os, sys                                                                  # Standard system level operations.
import filecmp                                                                  # Allows comparison of files (diff).
import time                                                                     # Used to determine name of file to save.
import shutil                                                                   # Used to backup existing settings.
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))

import config                                                                   # Imports all user configuration settings.
import lib                                                                      # Load in Hyperion related library helper functions.


#===================================================================================================
# OPEN LOG FILE AND WRITE SECTION HEADER FOR INSTANCE RUNNING SCRIPT
#---------------------------------------------------------------------------------------------------
if not os.path.exists(config.DIR_TMP):
    os.makedirs(config.DIR_TMP)

epoch=time.time()

f=open("%s/saving" % config.DIR_TMP, 'a')
f.write("\n\n\n\n====================================================================================================\n")
f.write("Saving request @ %s.\n" % str(time.strftime("%A, %B %d @ %H:%M:%S", time.localtime(epoch))) )


#===================================================================================================
# BREAK DOWN ABSOLUTE PATH TO HYPERION CONFIGURATION FILE INTO COMPONENTS THAT CAN BE USED FOR 
# PROCESSING THE CONFIGURATION DIRECTORY TO DETECT MOST RECENT SAVE FILES
#---------------------------------------------------------------------------------------------------
config_dir      = os.path.dirname(config.FPATH_HYPERION_CONFIG)
config_fname    = os.path.basename(config.FPATH_HYPERION_CONFIG)
config_ext      = os.path.splitext(config.FPATH_HYPERION_CONFIG)[1]


#===================================================================================================
# BACKUP EXISTING SETTINGS IF NOT ALREADY BACKED UP
#---------------------------------------------------------------------------------------------------
# Determine list of all potential backup files.
backups = []
for fname in os.listdir(config_dir):
    if fname != config_fname and fname.endswith(config_ext):
        backups.append(fname)

# Verify backup needed and execute backup.
if len(backups) > 0:
    f.write( "Located %s backups.\n" % str(len(backups)) )
    backups.sort(reverse=True)
    
    fpath_most_recent_backup = os.path.join(config_dir, backups[0])
    f.write( "Comparing current saved settings against most recent backup:  %s\n" % str(fpath_most_recent_backup) )
    
    # Note:  The filecmp.cmp() method will return 'True' if files are equal.
    if not filecmp.cmp(config.FPATH_HYPERION_CONFIG, fpath_most_recent_backup):
        f.write("Differences detected. Will create backup.\n")
        flag_backup = True
    else:
        f.write("No differences detected. Skipping backup step.\n")
        flag_backup = False
else:
    f.write( "No existing backups found. Will create the first backup.\n" )
    flag_backup = True

# Create backup.
if flag_backup == True:
    fpath_backup = os.path.join(config_dir, str(time.strftime("%Y%m%d-%H%M%S", time.localtime(epoch)))) + str(config_ext)
    shutil.copyfile(config.FPATH_HYPERION_CONFIG, fpath_backup)
    f.write( "Settings saved at:  %s\n" % fpath_backup)


#===================================================================================================
# DETERMINE SETTINGS TO SAVE
#---------------------------------------------------------------------------------------------------
saturation      = lib.query_param("saturationL")
luminance_gain  = lib.query_param("luminanceGain")
luminance_min   = lib.query_param("luminanceMin")
gamma_levels    = str(lib.query_param("gamma")).replace(",", "").split(" ")

f.write( "Settings that will be saved:\n")
f.write( "  - SaturationL:    %s\n" % str(saturation) )
f.write( "  - luminanceGain:  %s\n" % str(luminance_gain) )
f.write( "  - luminanceMin:   %s\n" % str(luminance_min) )
f.write( "  - gamma:          %s\n" % str(gamma_levels) )


#===================================================================================================
# EXECUTE SAVE TO CURRENT CONFIGURATION FILE
#---------------------------------------------------------------------------------------------------
# Opting to use 'sed -e' to replace entries in a template file rather than creative 'sed -i.bak', 
# awk, or other. This is clear cut and easiest to maintain. The second template file is not a huge 
# negative for this use case.
lib.send_command("sed   -e 's/${SATURATION_GAIN}/%s/g'  -e 's/${LUMINANCE_GAIN}/%s/g'   -e 's/${LUMINANCE_MIN}/%s/g'    -e 's/${GAMMA_RED}/%s/g'    -e 's/${GAMMA_GREEN}/%s/g'  -e 's/${GAMMA_BLUE}/%s/g'   %s > %s" %
                (       saturation,                     luminance_gain,                 luminance_min,                  gamma_levels[0],            gamma_levels[1],            gamma_levels[2],
                        config.FPATH_HYPERION_TEMPLATE, config.FPATH_HYPERION_CONFIG 
                ))


#===================================================================================================
# CLEANUP AND EXIT
#---------------------------------------------------------------------------------------------------
f.close()


