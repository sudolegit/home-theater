####################################################################################################
# @file
# 
# @brief            Contains library helper functions related to Hyperion.
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import os                                                                       # Used for interacting with the native file system.
import subprocess                                                               # Used when invoking Linux commands from a shell session.
import config                                                                   # Imports all user configuration settings.


####################################################################################################
# @brief            Execute query of current Hyperion values and return first value found.
# 
# @details          Uses Hyperion remote application to query the state of a running Hyperion 
#                   process.
# 
# @warning          Presumes 'value' conforms to the following structure:
#                       - "param" : #.#,
# 
# @param[in]        param               Parameter to query.
# 
# @retval           value               Current value for parameter.
####################################################################################################
def query_param(param):
    
    buff = send_command( "%s -l | grep %s | cut -d':' -f 2 | awk '{$1=$1};1' |  sed -e 's/,$//g' -e 's/^\[//g' -e 's/]$//g' | awk '{$1=$1};1'" % (config.HYPERION_REMOTE, param) )
    
    return buff[0]




####################################################################################################
# @brief            Execute query of default Hyperion values and return first value found.
# 
# @details          Uses Linux 'cat' and 'grep' commands to dump a the Hyperion JSON file and 
#                   isolate the line associated with the requested parameter string.
# 
# @warning          Presumes 'value' conforms to the following structure:
#                       - "param" : #.#,
# 
# @param[in]        param               Parameter to query.
# 
# @retval           value               Current value for parameter.
####################################################################################################
def query_param_default(param):
    
    buff = send_command( "cat  %s | grep %s | cut -d':' -f 2 | cut -d ',' -f 1 | awk '{$1=$1};1'" % (config.FPATH_HYPERION_CONFIG, param) )
    
    return buff[0]




####################################################################################################
# @brief            Execute provided command on the terminal and return the line-by-line results as 
#                   an array.
# 
# @details          Uses subprocess to tranmit a command to a shell terminal and capture all output.
# 
# @note             Presently, the return code is not provided (only the results returned to the 
#                   terminal).
# 
# @param[in]        cmd                 Command to execute.
# 
# @retval           results[]           Array where each entry represents a single line printed to 
#                                       to the terminal when the command was executed.
####################################################################################################
def send_command(cmd):
    
    # Execute command.
    proc    = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    
    # Loop through results line by line to strip carriage returns and convert to an array.
    buff=[]
    for line in proc.communicate():
        buff.append(line.strip())
    
    # Always strip last line as simply a line return character.
    buff.pop()
    
    # Return array of results.
    return buff




####################################################################################################
# @brief            Write the provided mode value to the tracking mode file.
#                   
# @details          Ensures the parent directory exists and blindly writes the mode to the mode 
#                   file. This will overwrite any existing setting.
# 
# @note             Mode value is blindly written as it is expected to be a base string that can be 
#                   used within mode increase and decrease scripts to locate which script to run 
#                   (e.g. 'mode_saturation_gain.py' should set mode of 'saturation_gain' so that 
#                   'saturation_gain/increase.py' and 'saturation_gain/decrease.py' can be invoked 
#                   by the 'mode/increase.py' and 'mode/decrease.py' scripts).
####################################################################################################
def mode_set(mode):
    
    if not os.path.exists(config.DIR_TMP):
        os.makedirs(config.DIR_TMP)
    
    f = open(config.PATH_MODE_FILE, 'w')
    f.write(mode)
    f.close()




####################################################################################################
# @brief            Wrapper to break down file name into a designated mode.
#                   
# @details          Extracts subname from filename provided and passes result to the 'mode_set()' 
#                   function to handle activation.
# 
# @returns          Passes back any return values from the 'mode_set()' function.
####################################################################################################
def mode_activate_by_fname(fname):
    
    fname_full      = os.path.basename(fname)
    fname_no_ext    = os.path.splitext(fname_full)[0]
    
    mode            = fname_no_ext.replace("activate_", "", 1)
    
    return mode_set(mode)




####################################################################################################
# @brief            Queries and returns the current mode value from the tracking file.
#                   
# @details          Ensures the tracking file exists and reads the first line of the tracking file.
# 
# @note             Mode value is blindly read as it is expected to be a base string that can be 
#                   used within mode increase and decrease scripts to locate which script to run 
#                   (e.g. 'mode_saturation_gain.py' should set mode of 'saturation_gain' so that 
#                   'saturation_gain/increase.py' and 'saturation_gain/decrease.py' can be invoked 
#                   by the 'mode/increase.py' and 'mode/decrease.py' scripts).
# 
# @retval           ""                  An empty string is returned when the mode has not been set 
#                                       or has been set to none.
# @retval           mode                String value containing the current mode [first line from 
#                                       tracking file].
####################################################################################################
def mode_get():
    
    mode = ""
    if os.path.exists(config.DIR_TMP):
        with open(config.PATH_MODE_FILE, 'r') as f:
            mode = f.readline() 
    
    return mode




####################################################################################################
# @brief            Attempts to execute the requested script [fname] provided for the active mode.
#                   
# @details          Confirms there is a non-empty active mode setting and that the mode's directory 
#                   contains a script named 'fname'. The 'fname' script is then invoked using the 
#                   'send_command()' function.
# 
# @param[in]        fname               Name of script to execute within the active mode's 
#                                       directory.
# 
# @returns          Passes back any return values from the 'send_command()' function.
####################################################################################################
def mode_action(fname):
    
    # Read in current mode value.
    mode = mode_get()
    
    # So long as mode is non-empty, see if any directories within the Hyperion scripts directory  
    # match the current mode [string match check]. If there is a match, look to see if the mode 
    # contains a file matching the provided action. Invoke the file if it exists.
    if mode != "":
        
        # Presumes all mode directories are parallel [within the same directory] as this script.
        base_path = os.path.dirname(os.path.realpath(__file__))
        for entry in os.listdir(base_path):
            
            path = os.path.join(base_path, entry)
            
            if os.path.isdir(path):
                
                if mode == entry:
                    
                    fpath = os.path.join(path, os.path.basename(fname))
                    
                    # Opting to invoke file manually [vs typical Python import method] as there 
                    # should be duplicates of files of the same name (minimally this script and 
                    # within the active mode directory.
                    if os.path.isfile(fpath):
                        return send_command("python %s" % fpath)




