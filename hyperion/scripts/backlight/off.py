####################################################################################################
# @file
# 
# @brief            Disable LED backlight by setting the minimum LED brightness level for the 
#                   Hyperion session to zero.
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import os, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))

import config                                                                   # Imports all user configuration settings.
import lib                                                                      # Load in Hyperion related library helper functions.


#===================================================================================================
# DISABLE BACKLIGHT VIA THE HYPERION-REMOTE COMMAND INTERFACE
#---------------------------------------------------------------------------------------------------
lib.send_command( "%s --luminanceMin %s" % (config.HYPERION_REMOTE, config.BACKLIGHT_OFF) )


