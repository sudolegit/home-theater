####################################################################################################
# @file
# 
# @brief            Enable LED backlight by setting the minimum LED brightness level for the 
#                   Hyperion session to the bare minimum (as defined in the configuration file).
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import os, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))

import config                                                                   # Imports all user configuration settings.
import lib                                                                      # Load in Hyperion related library helper functions.


#===================================================================================================
# ENABLE BACKLIGHT VIA THE HYPERION-REMOTE COMMAND INTERFACE
#---------------------------------------------------------------------------------------------------
lib.send_command( "%s --luminanceMin %s" % (config.HYPERION_REMOTE, config.BACKLIGHT_ON) )


