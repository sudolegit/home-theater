####################################################################################################
# @file
# 
# @brief            Toggle LED backlight by polling the current minimum LED brightness level and 
#                   changing between the backlight off and backlight on values (as defined in the 
#                   configuraiton file).
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import os, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))

import config                                                                   # Imports all user configuration settings.
import lib                                                                      # Load in Hyperion related library helper functions.


#===================================================================================================
# POLL CURRENT BACKLIGHT VALUE VIA THE HYPERION-REMOTE COMMAND INTERFACE
#---------------------------------------------------------------------------------------------------
backlight = lib.query_param("luminanceMin")


#===================================================================================================
# CHANGE BACKLIGHT VALUE VIA THE HYPERION-REMOTE COMMAND INTERFACE
#---------------------------------------------------------------------------------------------------
if float(backlight) == float(config.BACKLIGHT_OFF):
    lib.send_command( "%s --luminanceMin %s" % (config.HYPERION_REMOTE, config.BACKLIGHT_ON) )
else:
    lib.send_command( "%s --luminanceMin %s" % (config.HYPERION_REMOTE, config.BACKLIGHT_OFF) )


