####################################################################################################
# @file
# 
# @brief            Defines constants related to Hyperion.
####################################################################################################

#===================================================================================================                                                                                                               
# IMPORT DEPEDENCIES
#---------------------------------------------------------------------------------------------------                                                                                                               
import os                                                                                          # Required to check path information when defining installation dependent constants.                            
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   


#===================================================================================================                                                                                                               
# GENERAL PATH RELATED CONSTANTS                                                                                                                                                                                   
#---------------------------------------------------------------------------------------------------                                                                                                               
if os.path.isdir("/storage/home-theater"):                                                                                                                                                                         
    DIR_HYPERION_BIN        = "/storage/hyperion/bin"                                               # Directory containing all Hyperion binary files.                                                              
else:                                                                                                                                                                                                              
    DIR_HYPERION_BIN        = "/usr/share/hyperion/bin"                                             # Directory containing all Hyperion binary files.                                                              
                                                                                                                                                                                                                   
DIR_ROOT                = "/storage/home-theater"                                                   # Root directory for home theater install location.
DIR_TMP                 = "/tmp/home-theater"                                                       # Directory where all temp files for tracking behaviour is located.




#===================================================================================================
# DEFINE GENERAL HYPERION PATHS
#---------------------------------------------------------------------------------------------------
if os.path.isdir("/storage/home-theater"):                                                                                                                                                                         
    HYPERION                = "%s/hyperion.sh" % (DIR_HYPERION_BIN)                                 # Path to main Hyperion executable.
    HYPERION_REMOTE         = "%s/hyperion-remote.sh" % (DIR_HYPERION_BIN)                          # Path to Hyperion remote binary.
else:
    HYPERION                = "%s/hyperion" % (DIR_HYPERION_BIN)                                    # Path to main Hyperion executable.
    HYPERION_REMOTE         = "%s/hyperion-remote" % (DIR_HYPERION_BIN)                             # Path to Hyperion remote binary.

FPATH_HYPERION_CONFIG   = "%s/hyperion/settings/hyperion.config.json" % (DIR_ROOT)                  # Path to Hyperion configuration file.
FPATH_HYPERION_TEMPLATE = "%s/hyperion/settings/hyperion.config.template" % (DIR_ROOT)              # Path to Hyperion template file. Used when saving settings.




#===================================================================================================
# DEFINE MODE RELATED CONSTANTS
#---------------------------------------------------------------------------------------------------
PATH_MODE_FILE          = "%s/mode" % (DIR_TMP)



#===================================================================================================
# DEFINE HYPERION-REMOTE RELATED CONSTANTS
#---------------------------------------------------------------------------------------------------
BACKLIGHT_OFF           = "0.00"                                                                    # Set backlight threshold to 0 [off].
BACKLIGHT_ON            = "0.25"                                                                    # Set backlight threshold to bare minimum to be noticeable [on].

LUMINANCE_MIN           = "0.20"                                                                    # Minimum required luminance [brightness] value.
LUMINANCE_MAX           = "2.00"                                                                    # Maximum allowed luminance [brightness] value.
LUMINANCE_ADJ           = "0.10"                                                                    # Amount to increase or decrease luminance [brightness] by.

SATURATION_MIN          = "0.50"                                                                    # Minimum required saturation [color depth] value.
SATURATION_MAX          = "5.00"                                                                    # Maximum allowed saturation [color depth] value.
SATURATION_ADJ          = "0.10"                                                                    # Amount to increase or decrease saturation [color depth] by.

GAMMA_MIN               = "0.00"                                                                    # Minimum required gamma value when tuning RGB values.
GAMMA_MAX               = "5.00"                                                                    # Maximum allowed gamma value when tuning RGB values.
GAMMA_ADJ               = "0.10"                                                                    # Amount to increase or decrease gamma level when tuning RGB values.


