####################################################################################################
# @file
# 
# @brief            Reads in the current gamma RGB values and increments the value for 'green' by 
#                   the adjustment level defined in the configuration file until the allowed maximum 
#                   (also in configuration file) has been reached.
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import os, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))

import config                                                                   # Imports all user configuration settings.
import lib                                                                      # Load in Hyperion related library helper functions.


#===================================================================================================
# DEFINE SCRIPT CONSTANT(S)
#---------------------------------------------------------------------------------------------------
index       = 1                                                                 # Index into values array:  [R, G, B].
param_get   = "gamma"                                                           # Getter string to use when querying value.
param_set   = "gamma"                                                           # Setter string to use when updating value.


#===================================================================================================
# DETERMINE NEW VALUE TO APPLY
#---------------------------------------------------------------------------------------------------
crnt_vals   = []
for val in lib.query_param(param_get).split(','):
    crnt_vals.append( val.strip() )

new_vals = crnt_vals[:]

new_vals[index] = "%.3f" % min( (float(new_vals[index]) + float(config.GAMMA_ADJ)), float(config.GAMMA_MAX) )


#===================================================================================================
# APPLY VALUE IF CHANGE DETECTED
#---------------------------------------------------------------------------------------------------
if crnt_vals[index] != new_vals[index]:
    lib.send_command( "%s --%s '%s'" % (config.HYPERION_REMOTE, param_set, " ".join(new_vals)) )


