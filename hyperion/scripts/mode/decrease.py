####################################################################################################
# @file
# 
# @brief            Invokes the management file to 'decrease' an active mode setting.
# 
# @details          Confirms there is a valid active mode and invokes the mode's corresponding 
#                   'decrease.py' script [if one exists]. Actual increasing / limiting handled 
#                   by invoked script.
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import os, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))

import lib                                                                      # Load in Hyperion related library helper functions.


#===================================================================================================
# ATTEMPT TO INVOKE A SCRIPT MATCHING THE NAME OF THIS FILE WITHIN THE ACTIVE MODE'S DIRECTORY
#---------------------------------------------------------------------------------------------------
lib.mode_action(os.path.basename(__file__))


