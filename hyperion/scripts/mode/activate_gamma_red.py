####################################################################################################
# @file
# 
# @brief            Sets the active mode for tuning Hyperion to 'gamma_red'.
# 
# @note             Once a mode is set, the mode 'increase.py' and 'decrease.py' scripts can be 
#                   applied.
####################################################################################################


#===================================================================================================
# IMPORT EXTERNAL MODULES
#---------------------------------------------------------------------------------------------------
import os, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))

import lib                                                                      # Load in Hyperion related library helper functions.


#===================================================================================================
# ACTIVATE MODE
#---------------------------------------------------------------------------------------------------
lib.mode_activate_by_fname(__file__)


