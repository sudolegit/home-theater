#!/bin/bash
####################################################################################################
# @file
# 
# @brief			Main initialization script for Hyperion. Intended to be invoked from systemd 
#					event.
####################################################################################################


#===================================================================================================
# DEFINE CONSTANTS USED IN SCRIPT.
#---------------------------------------------------------------------------------------------------
DIR_HOME_THEATER="/opt/home-theater"											# Directory where the home-theater repository is installed.


#===================================================================================================
# ENABLE CONTROL OF LED STRIP. THIS WILL CONFIGURE THE GPIO AND DEFAULT TO ENABLING THE LED.
#---------------------------------------------------------------------------------------------------
python ${DIR_HOME_THEATER}/led-strip/init.py


#===================================================================================================
# START BACKGROUND PROCESS THAT WILL DISABLE THE LED STRIP AFTER 10 SECONDS. THIS SHOULD BE ENOUGH
# TIME TO ALLOW HYPERION TO LAUNCH AND PLAY THE INITIAL LED NOTIFICATION STREAM.
#---------------------------------------------------------------------------------------------------
## Disabling sleep until keyboard hooks developed ****>>>> sleep 10 && python ${DIR_HOME_THEATER}/led-strip/off.py &


#===================================================================================================
# LAUNCH HYPERION. THIS IS THE APPLICATION WHICH CONTROLS THE LED STRIP.
#---------------------------------------------------------------------------------------------------
/usr/bin/hyperiond ${DIR_HOME_THEATER}/hyperion/settings/hyperion.config.json


