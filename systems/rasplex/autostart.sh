#!/bin/bash
####################################################################################################
# @file
# 
# @brief			Main initialization script run every boot automatically. Used to add custom 
#					scripts and apps into the active runtime session.
####################################################################################################


#===================================================================================================
# DEFINE CONSTANTS USED IN SCRIPT.
#---------------------------------------------------------------------------------------------------
DIR_HOME_THEATER="/opt/home-theater"											# Directory where the home-theater repository is installed.


#===================================================================================================
# ENABLE CONTROL OF LED STRIP. THIS WILL CONFIGURE THE GPIO AND DEFAULT TO ENABLING THE LED.
#---------------------------------------------------------------------------------------------------
python ${DIR_HOME_THEATER}/led-strip/init.py


#===================================================================================================
# LAUNCH HYPERION. THIS IS THE APPLICATION WHICH CONTROLS THE LED STRIP.
#---------------------------------------------------------------------------------------------------
/storage/hyperion/bin/hyperiond.sh ${DIR_HOME_THEATER}/hyperion/settings/hyperion.config.json > /storage/logfiles/hyperion.log 2>&1 &


#===================================================================================================
# ALLOW HYPERION TO SETTLE AND INITIAL LED DEMO TO PLAY BEFORE DISABLING.
#---------------------------------------------------------------------------------------------------
sleep 5
python ${DIR_HOME_THEATER}/led-strip/off.py


