Aim
----------------------------------------------------------------------------------------------------
This project contains general scripts and configuration files associated with home theater related 
systems.




Structure
----------------------------------------------------------------------------------------------------
A rough overview of top-level folder structure is as follows:
    
    -   <hyperion>
        -   Configuration file for default settings and helper scripts related to managing an active 
            Hyperion session.
    
    -   <led-strip>
        -   Scripts used to enable or disable the external LED strip (not related to managing 
            Hyperion, just LED strip control).
    
    -   <sandbox>
        -   Random scripts created while exploring system settings and determining how to implement 
            desired features.
    
    -   <systems>
        -   Contains a single subfolder for each and every system supported by this project.
        -   Each subfolder contains bootup scripts and any custom configuration files (e.g. 
            autostart.sh and keyboard.xml).




Installation
----------------------------------------------------------------------------------------------------
A.  OSMC
    
    1.  Create a functional OSMC installation.
    
    2.  Install Hyperion using the auto-install script provided with the Hyperion project.
    
    3.  Clone or copy this project into:
        -   /opt/home-theater
    
    4.  Remove the following files created during the install process of OSMC && Hyperion [IFF they 
        exist]:
        -   /home/osmc/.kodi/userdata/keymaps/keyboard.xml
        -   /etc/systemd/system/hyperion.service
    
    5.  Run the following commands to link in the home theater settings:
        -   ln -s /opt/home-theater/systems/osmc/keyboard.xml           /home/osmc/.kodi/userdata/keymaps/keyboard.xml
        -   sudo chmod +x /opt/home-theater/systems/osmc/launch-hyperion.sh
        -   sudo ln -s /opt/home-theater/systems/osmc/50-gpio.rules     /etc/udev/rules.d/50-gpio.rules
        -   sudo ln -s /opt/home-theater/systems/osmc/hyperion.service  /etc/systemd/system/hyperion.service
        -   sudo systemctl daemon-reload
        -   sync


B.  Raspbian
    
    1.  Create a functional Raspbian installation.
    
    2.  Install Hyperion using the auto-install script provided with the Hyperion project.
    
    3.  Clone or copy this project into:
        -   /opt/home-theater
    
    4.  Remove the following files created during the install process of OSMC && Hyperion [IFF they 
        exist]:
        -   /etc/systemd/system/hyperion.service
    
    5.  Run the following commands to link in the home theater settings:
        -   sudo chmod +x /opt/home-theater/systems/raspbian/launch-hyperion.sh
        -   sudo ln -s /opt/home-theater/systems/raspbian/hyperion.service  /etc/systemd/system/hyperion.service
        -   sudo systemctl daemon-reload
        -   sync
    

C.  RasPlex
    
    1.  Create a functional RasPlex installation.
    
    2.  Install Hyperion using the auto-install script provided with the Hyperion project.
    
    3.  Clone or copy this project into:
        -   /opt/home-theater
    
    4.  Remove the following files created during the install process of RasPlex && Hyperion [IFF 
        they exist]:
        -   /storage/.config/autostart.sh
        -   /storage/.kodi/userdata/keymaps/keyboard.xml
    
    5.  Run the following commands to link in the home theater settings:
        -   ln -s /opt/home-theater/systems/rasplex/autostart.sh    /storage/.config/autostart.sh
        -   ln -s /opt/home-theater/systems/rasplex/keyboard.xml    /storage/.kodi/userdata/keymaps/keyboard.xml
        -   sync


D.  RetroPie
    
    1.  Create a functional RetroPie installation.
    
    2.  Install Hyperion using the auto-install script provided with the Hyperion project.
    
    3.  Clone or copy this project into:
        -   /opt/home-theater
    
    4.  Remove the following files created during the install process of RetroPie && Hyperion [IFF they 
        exist]:
        -   /etc/systemd/system/hyperion.service
    
    5.  Run the following commands to link in the home theater settings:
        -   sudo chmod +x /opt/home-theater/systems/retropie/launch-hyperion.sh
        -   sudo ln -s /opt/home-theater/systems/retropie/hyperion.service  /etc/systemd/system/hyperion.service
        -   sudo systemctl daemon-reload
        -   sync


